﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Default Enemy", menuName = "Enemies/Default")]
public class EnemyDefault : EnemyObject
{
    public void Awake()
    {
        type = EnemyType.Default;
    }
}
