﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Armored Enemy", menuName = "Enemies/Armored")]
public class EnemyArmored : EnemyObject
{
    private void Awake()
    {
        type = EnemyType.Armored;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
