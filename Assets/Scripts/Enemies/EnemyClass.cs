﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyClass : MonoBehaviour, IDamageable<int>, IKillable
{
    //hur snabbt fienden rör sig
    [SerializeField]
    private float speed;

    //hur mycket hp fienden har
    [SerializeField]
    private int health;

    private Waypoints wp;

    private int waypointIndex;

    //hur mycket fienden skadar spelaren
    private int playerDamageAmount;

    private GameHandler gameHandler;
    // Start is called before the first frame update
    void Start()
    {
        wp = GameObject.FindGameObjectWithTag("Waypoint").GetComponent<Waypoints>();
    }
    void Awake()
    {
        gameHandler = FindObjectOfType<GameHandler>();
        gameHandler.OnLoadGame += GameHandler_OnLoadGame;
    }

    private void GameHandler_OnLoadGame(object sender, EventArgs e)
    {
        Clear();
    }

    public static List<EnemyClass> enemyList = new List<EnemyClass>();
    /*public static EnemyClass GetClosestEnemy(Vector3 position, float maxRange)
    {
        EnemyClass closest = null;
        foreach(EnemyClass enemy in enemyList)
    }*/

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, wp.waypoints[waypointIndex].position, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, wp.waypoints[waypointIndex].position) < 0.1f)
        {
            if (waypointIndex < wp.waypoints.Length - 1)
            {
                waypointIndex++;
            }
            else
            {
                DamagePlayer();
            }
        }

        Vector3 direction = wp.waypoints[waypointIndex].position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    private void DamagePlayer()
    {
        Debug.Log("Player damaged");
        GameHandler gameHandler = GameObject.Find("GameHandler").GetComponent<GameHandler>();
        //gameHandler.EnemyDefeated();

        Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player.Damage(playerDamageAmount);
        gameHandler.PlayerDamaged(playerDamageAmount);
        Kill();
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    public void Damage(int damageTaken)
    {
        health -= damageTaken;

        if (health <= 0)
        {
            GameHandler gameHandler = GameObject.Find("GameHandler").GetComponent<GameHandler>();
            gameHandler.EnemyDefeated();
            Kill();
        }
    }
    public void Setup(EnemyObject enemyObject)
    {
        speed = enemyObject.moveSpeed;
        playerDamageAmount = enemyObject.playerDamageAmount;
        GetComponentInChildren<SpriteRenderer>().sprite = enemyObject.sprite;
        GetComponentInChildren<SpriteRenderer>().transform.position = transform.position;
        health = enemyObject.health;
    }

    public void Upgrade(int upgradeAmount)
    {
        speed += upgradeAmount;
        health += upgradeAmount;
    }
    public void Clear()
    {
        Destroy(gameObject);
    }
}
