﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class TowerSlot : MonoBehaviour, IDropHandler
{
    //detta script är för "slots" där man ska dra nya torn som man kan sätta ut på banan

    [SerializeField]
    private Transform tower;
    [SerializeField]
    private Transform slot;
    private CanvasGroup canvasGroup;

    private GameHandler gameHandler;

    private TowerDrag towerDrag;
    private TowerDragManager dragManager;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        gameHandler = GameObject.FindObjectOfType<GameHandler>();
    }
    public void OnDrop(PointerEventData eventData)
    {
        towerDrag = eventData.pointerDrag.GetComponent<TowerDrag>();
        Debug.Log("Drop");
        //om man kan bygga ett torn (alltså har tillräckligt med scrap)
        if (gameHandler.canBuildTower)
        {
            //om det finns ett objekt i slotten
            if (eventData.pointerDrag != null)
            {
                //fäst objektets position in i "slot" positionen
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().localPosition;

                //ersätt drag objektet med ett faktiskt torn objekt
                Transform towerClone = Instantiate(tower, slot.position, slot.rotation);
                if (towerClone != null)
                {
                    gameHandler.lowerScrap();
                    //drag objektet förstörs (tornet ersätter det)
                    Destroy(eventData.pointerDrag);
                    //du kan inte dra in fler torn på samma ställe
                    canvasGroup.blocksRaycasts = false;
                }
            }
        }
        else
        {
            //om man inte har tillräckligt med scrap för att bygga ett torn åker towerdrag tillbaka till toppen av skärmen
            Debug.Log("Not enough scrap");
            towerDrag.returnToPosition();
            return;
        }
    }
}
