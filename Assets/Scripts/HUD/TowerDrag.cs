﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerDrag : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField]
    private Canvas canvas;
    private CanvasGroup canvasGroup;

    private RectTransform rectTransform;

    private List<TowerSlot> towerSlots;

    private Vector2 startingPosition;
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        towerSlots = new List<TowerSlot>();
        towerSlots.AddRange(GameObject.FindObjectsOfType<TowerSlot>());
        startingPosition = rectTransform.anchoredPosition;
        Debug.Log(name + " position: " + rectTransform.anchoredPosition);
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Begin drag");
        //medans du drar tornet ska dess alpha delvis sänkas
        canvasGroup.alpha = .6f;
        //ska ej blocka raycasts så man kan droppa tornet i slots
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //dra tornet längs musen
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor; //delta är mängden musen har rört sig
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Pointer down");
    }

    public void returnToPosition()
    {
        Debug.Log("Return to position");
        rectTransform.anchoredPosition = startingPosition;
    }
}
