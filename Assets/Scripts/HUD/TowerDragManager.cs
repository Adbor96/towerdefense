﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDragManager : MonoBehaviour
{
    //håll reda på hur mycket scrap man har per frame
    //man får över 10 scrap, då kan man låsa upp ett torn
    //att man har över 10 betyder inte att man ska låsa upp ett torn

    [SerializeField]
    private List<GameObject> towerDragList;


    private GameHandler gamehandler;

    private bool towerIsUnlocked;

    private TowerSlot towerSlot;
    // Start is called before the first frame update
    void Start()
    {
        gamehandler = GameObject.FindObjectOfType<GameHandler>();
        gamehandler.OnScrapReached += Gamehandler_OnScrapReached;
        gamehandler.OnNotEnoughScrap += Gamehandler_OnNotEnoughScrap;
    }

    private void Gamehandler_OnNotEnoughScrap(object sender, System.EventArgs e)
    {
        LockTowers();
    }

    private void Gamehandler_OnScrapReached(object sender, System.EventArgs e)
    {
        UnlockTowers();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.L))
        {
            LockTowers();
        }
    }
    private void UnlockTowers()
    {
        //kollar varje torn i listan
        /*foreach (GameObject tower in towerDragList)
        {
            //om tornet är inaktivt
            if (!tower.activeInHierarchy)
            {
                    //sätt ett som aktivt
                    //towerDragList[0].SetActive(true);
                    tower.SetActive(true);
                    towerIsUnlocked = false;
                    //towerDragList.Remove(tower);
                    return;

            }
        }*/

        for (int i = 0; i < towerDragList.Count; i++)
        {
            foreach (GameObject _tower in towerDragList)
            {
                if (!_tower.activeInHierarchy)
                {
                    towerDragList[i].SetActive(true);
                    towerDragList.RemoveAt(i);
                    return;
                }
            }
        }
    }
    private void LockTowers()
    {
        //kollar varje torn i listan
        foreach (GameObject tower in towerDragList)
        {
            //om tornet är aktivt
            if (tower.activeInHierarchy)
            {
                //sätt ett som inaktivt
                tower.SetActive(false);
                return;
            }
        }
    }
}
