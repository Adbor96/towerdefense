﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasText : MonoBehaviour
{
    [SerializeField]
    private Text playerHpText;
    [SerializeField]
    private Text enemyWaveCounter;
    [SerializeField]
    private Text enemyCounter;
    [SerializeField]
    private Text scrapCounter;


    private GameHandler gameHandler;

    // Start is called before the first frame update
    void Awake()
    {
        gameHandler = GameObject.FindObjectOfType<GameHandler>();
        gameHandler.OnPlayerHealthChanged += GameHandler_OnPlayerHealthChanged;
        gameHandler.OnEnemyAmountChanged += GameHandler_OnEnemyAmountChanged;
        gameHandler.OnScrapAmountChanged += GameHandler_OnScrapAmountChanged;
        gameHandler.OnScrapReached += GameHandler_OnScrapReached;
        gameHandler.onWaveChange += GameHandler_onWaveChange;
    }

    private void GameHandler_onWaveChange(int wave)
    {
        enemyWaveCounter.text = "Enemy Wave \n" + wave.ToString();
    }

    private void GameHandler_OnScrapReached(object sender, System.EventArgs e)
    {
        //throw new System.NotImplementedException();
    }

    private void GameHandler_OnScrapAmountChanged(int scrapChangeAmount)
    {
        scrapCounter.text = "Scrap \n" + scrapChangeAmount.ToString();
    }

    private void GameHandler_OnEnemyAmountChanged(int _remainingEnemies, int _enemiesMax)
    {
        enemyCounter.text = "Enemies remaining \n" + _remainingEnemies.ToString() + "/ " + _enemiesMax;
    }

    private void GameHandler_OnPlayerHealthChanged(int playerHealthAmount)
    {
        playerHpText.text = "HP \n" + playerHealthAmount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
