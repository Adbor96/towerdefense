﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamageable<int>, IKillable
{
    [SerializeField]
    private int health;

    private SpriteRenderer playerSprite;
    // Start is called before the first frame update
    void Start()
    {
        playerSprite = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Damage(1);
        }
        if (health <= 0)
        {
            Kill();
        }
    }
    public void Damage(int damageTaken)
    {
        health -= damageTaken;
    }

    public void Kill()
    {
        playerSprite.enabled = false;
        Debug.Log("Dead");
    }
}
