﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable<DamageTaken>
{
    void Damage(DamageTaken damageTaken);

}
public interface IKillable
{
    void Kill();
}
