﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData 
{
    public int enemiesRemaining;
    public int enemiesMax;
    public int enemyWave;
    public int playerHealth;
    public int scrap;
    public int amountOfEnemiesToSpawn;
    //en lista av gameObjects (detta är våra torn)
    public TowerSaveData[] towers;
    

    public SaveData (GameHandler gameHandler)
    {
        enemyWave = gameHandler.enemyWave;
        playerHealth = gameHandler.playerHp;
        scrap = gameHandler.currentScrap;
        enemiesRemaining = gameHandler.enemiesRemaining;
        enemiesMax = gameHandler.enemiesMax;
        amountOfEnemiesToSpawn = gameHandler.amountOfEnemiesToSpawn;
        //hitta alla torn skript i banan
        Tower[] _towers = GameObject.FindObjectsOfType<Tower>();
        //skapa en tom array av towerSaveData som är lika stor som hur många torn det finns i banan
        towers = new TowerSaveData[_towers.Length];
        //loopa igenom alla platser i array och fyller de med torn
        for (int i = 0; i < towers.Length; i++)
        {
            towers[i] = new TowerSaveData(_towers[i]);
        }
    }
}

[System.Serializable]
public class TowerSaveData
{
    public float[] position;
    public float positionX;
    public float positionY;
    public int towerLevel;

    public TowerSaveData (Tower inputTower)
    {
        position = new float[2];
        position[0] = inputTower.transform.position.x;
        position[1] = inputTower.transform.position.y;
        positionX = inputTower.transform.position.x;
        positionY = inputTower.transform.position.y;
        towerLevel = inputTower.towerLevel;
    }
}
