﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Default,
    Flying,
    Armored
}
[CreateAssetMenu(fileName = "New Enemy Object", menuName = "Enemies/EnemyObject")]
public class EnemyObject : ScriptableObject
{
    public Sprite sprite;
    public int health;
    public float moveSpeed;
    public int playerDamageAmount;
    public EnemyType type;

}
