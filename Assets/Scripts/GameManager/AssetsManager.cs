﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsManager : MonoBehaviour
{
    private static AssetsManager _i;

    public static AssetsManager i
    {
        get
        {
            if (_i == null) _i = Instantiate(Resources.Load<AssetsManager>("AssetsManager"));       
            return _i;
        }
    }

    public Transform pfProjectile;
    public Transform enemy01;
}
