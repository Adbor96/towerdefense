﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{
    //ATT GÖRA
    //måste ha en tower prefab

    //var fiender spawnas
    private Vector3 enemySpawnPos;

    //den våg spelaren är på
    public int enemyWave;

    //int för hur mycket fienders stats ska uppgradras relaterat till rundan
    private int upgradeAmount;

    //hur många fiender det är i nuvarande vågen
    public int enemiesMax;

    //hur många fiender det finns kvar i nuvarande vågen
    public int enemiesRemaining;

    //hur många fiender finns på skärmen just nu
    private int currentEnemies;

    //hur många fiender som ska spawnas max per våg
    public int amountOfEnemiesToSpawn;

    //hur lång tid det tar för en ny våg att komma
    private float waveCountdown;
    private float waveCountdownMax;

    //hur lång tid det tar mellan att två fiender spawnas
    private float spawnCountdown;
    private float spawnCountdownMax;

    //visar spelarens hp
    public int playerHp;
    //private Text playerHpText; visuella ska ske i separat script

    //hur mycket "scrap" spelaren har (scrap samlas när en fiende dör och används för att bygga torn)
    public int currentScrap;
    //hur mycket scrap som man får när en fiende blir besegrad
    private int scrapGained;
    //den mängd scrap som krävs för att bygga ett nytt torn
    private int scrapUpgradeAmount;
    //hur mycket scrap spelaren har när den har byggt ett nytt torn

    //fienden som ska spawnas
    public Transform enemy;

    private EnemyClass _enemy;

    public List<EnemyObject> enemyObjectList;

    private TowerSlot towerSlot;

    public bool canBuildTower;

    [SerializeField]
    private Transform towerPrefab;

    //pause menu
    [SerializeField]
    private GameObject pausePanel;
    private bool isPaused;

    // Start is called before the first frame update
    void Start()
    {
        //vart fiender ska spawnas
        enemySpawnPos = transform.Find("EnemySpawnPos").position;

        //vi är på första vågen

        waveCountdownMax = 5;
        waveCountdown = waveCountdownMax;

        spawnCountdownMax = 3;
        spawnCountdown = spawnCountdownMax;

        //_enemy = new EnemyClass().GetComponent<EnemyClass>();

        //enemiesMax = 4;
        //enemiesRemaining = enemiesMax;

        currentScrap = 0;

        playerHp = 3;
        RoundStart();
        scrapGained = 3;

        towerSlot = GameObject.FindObjectOfType<TowerSlot>();
    }
        
    public void lowerScrap()
    {
        currentScrap -= 10;
        OnScrapAmountChanged?.Invoke(currentScrap);
    }

    //när spelaren skadas uppdatera texten som visar hur mycket HP spelaren har kvar
    private void _enemy_OnPlayerDamaged(int playerDamageAmount)
    {
        playerHp -= playerDamageAmount;
    }


    // Update is called once per frame
    void Update()
    {
        //playerHpText.text = playerHp.ToString();
        if (enemy != null)
        {
            //Debug.Log("Enemy is not null");
            _enemy = enemy.GetComponent<EnemyClass>();
        }
        spawnCountdown += spawnCountdownMax * Time.deltaTime;
        if (spawnCountdown >= spawnCountdownMax)
        {
            spawnCountdown = spawnCountdownMax;
        }

        //medans det fortfarande finns fiender kvar i vågen skall fiender spawnas i jämna mellanrum
        if (amountOfEnemiesToSpawn > 0)
        {
            if (spawnCountdown >= spawnCountdownMax)
            {
                SpawnEnemy();
                spawnCountdown = 0;
            }
        }
        //mängden fiender skall inte gå under 0
        if (enemiesRemaining <= 0)
        {
            enemiesRemaining = 0;
            //om det finns 0 fiender kvar, räkna ner till en ny våg
            if (currentEnemies <= 0)
            waveCountdown -=  Time.deltaTime;

            if (waveCountdown <= 0)
            {
                //nästa våg startar
                RoundStart();
                waveCountdown = waveCountdownMax;
            }
        }
        if (currentEnemies <= 0)
        {
            currentEnemies = 0;
        }
        if (currentScrap >= 10)
        {
            OnScrapReached?.Invoke(this, EventArgs.Empty);
            canBuildTower = true;
        }
        else 
        {
            //OnNotEnoughScrap?.Invoke(this, EventArgs.Empty);
            canBuildTower = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            enemyWave++;
        }
        upgradeAmount = enemyWave - 2;

        //pause
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(!isPaused)
            PauseGame();

            else
            {
                UnPause();
            }
        }
        if (playerHp <= 0)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
    void SpawnEnemy()
    {
        //instantiate en fiende
        //EnemyClass _enemy = new EnemyClass();
        Transform spawnedEnemy = Instantiate(enemy, enemySpawnPos, enemy.rotation);
        if (enemyWave == 1)
        {
            spawnedEnemy.GetComponent<EnemyClass>().Setup(enemyObjectList[0]);
        }
                if (enemyWave == 2)
        {
            spawnedEnemy.GetComponent<EnemyClass>().Setup(enemyObjectList[1]);
        }
        if (enemyWave > 2)
        {
            int randomEnemyType = UnityEngine.Random.Range(0, 2);
            spawnedEnemy.GetComponent<EnemyClass>().Setup(enemyObjectList[randomEnemyType]);
            spawnedEnemy.GetComponent<EnemyClass>().Upgrade(upgradeAmount);
            Debug.Log("Spawning random enemies");
        }
        amountOfEnemiesToSpawn--;
        currentEnemies++;
    }

    void RoundStart()
    {
        //när en runda startar
        //spelaren är på en ny våg av fiender
        enemyWave++;
        //mängden fiender i en våg ökar
        enemiesMax += 3;
        //ställ om hur många fiender det finns att besegra
        enemiesRemaining = enemiesMax;
        amountOfEnemiesToSpawn = enemiesMax;
        onWaveChange?.Invoke(enemyWave);

        OnEnemyAmountChanged?.Invoke(enemiesRemaining, enemiesMax);
        OnScrapAmountChanged?.Invoke(currentScrap);
    }

    public void EnemyDefeated()
    {
        Debug.Log("Enemy defeated");
        currentEnemies--;
        enemiesRemaining--;
        currentScrap += scrapGained;
        OnScrapAmountChanged?.Invoke(currentScrap);
        OnEnemyAmountChanged?.Invoke(enemiesRemaining, enemiesMax);
    }
    public void PlayerDamaged(int damageAmount)
    {
        playerHp -= damageAmount;
        OnPlayerHealthChanged?.Invoke(playerHp);
        enemiesRemaining--;
        currentEnemies--;
        OnEnemyAmountChanged?.Invoke(enemiesRemaining, enemiesMax);
    }

    //events
    //när spelaren har en viss mängd scrap kan man bygga ett torn
    public event EventHandler OnScrapReached;
    //när spelaren inte längre har tillräckligt med scrap
    public event EventHandler OnNotEnoughScrap;

    public delegate void ScrapChangedDelegate(int scrapChangeAmount);
    public event ScrapChangedDelegate OnScrapAmountChanged;

    //när spelaren blir skadad
    public delegate void playerHealthChangedDelegate(int playerHealthAmount);
    public event playerHealthChangedDelegate OnPlayerHealthChanged;

    //event om hur många fiender det finns och vilken våg spelaren är på
    public delegate void enemiesRemainingDelegate(int _remainingEnemies, int _enemiesMax);
    public event enemiesRemainingDelegate OnEnemyAmountChanged;

    //event om vilken våg spelaren är på
    public delegate void waveDelegate(int wave);
    public event waveDelegate onWaveChange;

    //event om att förstöra alla fiender på banan
    public event EventHandler OnLoadGame;


    public void PauseGame()
    {
        isPaused = true;
        pausePanel.SetActive(true);
        Time.timeScale = 0;
    }
    public void UnPause()
    {
        isPaused = false;
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    //SAVING
    public void SaveLevel()
    {
        SaveSystem.SaveLevel(this);
    }
    public void LoadLevel()
    {
        SaveData data = SaveSystem.LoadPlayer();

        enemyWave = data.enemyWave;
        playerHp = data.playerHealth;
        currentScrap = data.scrap;
        enemiesRemaining = data.enemiesMax;
        enemiesMax = data.enemiesMax;
        currentEnemies = 0;
        amountOfEnemiesToSpawn = data.enemiesMax;
        waveCountdown = waveCountdownMax;
        OnPlayerHealthChanged?.Invoke(playerHp);
        OnEnemyAmountChanged?.Invoke(data.enemiesRemaining, data.enemiesMax);
        OnScrapAmountChanged?.Invoke(currentScrap);
        onWaveChange?.Invoke(enemyWave);
        OnLoadGame?.Invoke(this, EventArgs.Empty);
        EnemyClass[] enemies = FindObjectsOfType<EnemyClass>();
        for (int i = 0; i < data.towers.Length; i++)
        {
            Vector2 position;
            position.x = data.towers[i].position[0];
            position.y = data.towers[i].position[1];
            Instantiate(towerPrefab, position, Quaternion.identity);
        }
    }
}
