﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField]
    private Transform exit;

    private Vector3 projectileExit;

    [SerializeField]
    private float shootingCooldown;
    private float shootingCooldownMax;

    public Transform enemyPosition;

    [SerializeField]
    private GameObject pfProjectile;

    private List<EnemyClass> enemyList;

    public int towerLevel;

    // Start is called before the first frame update
    void Start()
    {
        //projectileExit = transform.Find("ProjectileExit").position; //funkar rakt av inte, helt annat ställe än var den egentligen är
        projectileExit = exit.transform.position;
        shootingCooldownMax = 1f;
        shootingCooldown = shootingCooldownMax;
        enemyList = new List<EnemyClass>();
    }

    // Update is called once per frame
    void Update()
    {
        //STARTA OM KODNING

        //cooldownen på att skjuta ska räknas upp
        shootingCooldown += Time.deltaTime;
        //cooldownen ska inte gå över max
        if (shootingCooldown >= shootingCooldownMax)
        {
            shootingCooldown = shootingCooldownMax;
        }
        //om tornet ser en fiende
        if (enemyPosition != null)
        {
            //om tornet kan skjuta
            if (shootingCooldown >= shootingCooldownMax)
            {
                //skjut
                Shoot(projectileExit, enemyPosition.position);
                //ställ om cooldown
                shootingCooldown = 0;
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnemyClass enemy = collision.GetComponent<EnemyClass>();
        //om fiender befinner sig inuti tornets syn
        if (enemy != null)
        {
            //Debug.Log("Contact");
            //fienden läggs till i en lista
            enemyList.Add(enemy);

            //följ positionen av första fienden i listan
            enemyPosition = enemyList[0].transform;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        EnemyClass enemy = collision.GetComponent<EnemyClass>();
        //om fienden lämnar tornets syn
        if (enemy != null)
        {
            //Debug.Log("Leave");
            //fienden tas bort från listan
            enemyList.Remove(enemy);

            //kolla om listan är tom
            bool isEmpty = !enemyList.Any();

            //om listan är tom
            if (isEmpty)
            {
                //det finns inget att söka på
                enemyPosition = null;
            }
            else
            {
                //annars siktar tornet på nästa fiende i listan (om det finns någon kvar)
                enemyPosition = enemyList[0].transform;
            }

        }
    }
    public void Shoot(Vector3 spawnPosition, Vector3 targetPosition) //varför funkar inte static?
    {
        //projektilen instantiate:as
        GameObject projectileTransform = Instantiate(pfProjectile, spawnPosition, pfProjectile.transform.rotation);

        //säg till kulan vart den ska röra sig, hur snabbt och hur mycket skada den ska göra
        Projectile bullet = projectileTransform.GetComponent<Projectile>();
        bullet.Setup(enemyPosition, 10f, 1);
    }
}
