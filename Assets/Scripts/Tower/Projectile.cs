﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //var fienden kulan ska söka efter är
    private Transform enemyTransform;

    private float moveSpeed;

    private int bulletDamage;

    /*public static void Create(Vector3 spawnPosition, Vector3 targetPosition)
    {
       Transform projectileTransform = Instantiate(AssetsManager.i.pfProjectile, spawnPosition, quaternion.identity);

        Projectile projectile = projectileTransform.GetComponent<Projectile>();
        projectile.Setup(targetPosition);
    }*/
    private void Awake()
    {
       
    }

    public void Setup(Transform _enemyTransform, float _moveSpeed, int _bulletDamage)
    {
        enemyTransform = _enemyTransform;
        moveSpeed = _moveSpeed;
        bulletDamage = _bulletDamage;
    }
    private void Update()
    {
        /*Vector3 moveDirection = (targetPosition - transform.position).normalized;
        float moveSpeed = 50f;
        transform.position = moveDirection * moveSpeed * Time.deltaTime;

        float angle = getAngleFromVectorFloat(moveDirection);
        transform.eulerAngles = new Vector3(0, 0, angle);

        float destroySelfDistance = 1f;
        if (Vector3.Distance(transform.position, targetPosition) < destroySelfDistance)
        {
            Destroy(gameObject);
        }*/

        //om det inte finns en fiende
        if (enemyTransform == null)
        {
            //förstör kulan och gå tillbaka för att kolla efter fiender
            Destroy(gameObject);
            return;
        }

        transform.position = Vector3.MoveTowards(transform.position, enemyTransform.position, moveSpeed * Time.deltaTime);
        //projektilen rör sig mot fienden
    }

    public static float getAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;

        return n;
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        EnemyClass enemy = collision.GetComponent<EnemyClass>();

        if (enemy != null)
        {
            enemy.Damage(bulletDamage);
            Destroy(gameObject);
        }
    }
}
